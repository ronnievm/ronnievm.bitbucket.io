/**
 * Function to return a random integer number based on its parameters.
 * @ param min The floor of the random result.
 * @ param max The ceiling of the random result.
 * @ return A random number between the min and max parameters.
*/
function randomMinMax(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}