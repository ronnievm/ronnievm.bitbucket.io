/// <reference path="game.ts" />

/**
 * Creates a new game, and runs it by calling its init function.
*/
let game = new Game();
game.init();