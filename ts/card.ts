/**
 * Card class which represents the playable cards in the game.
 * Contains many objects, arrays and other fields which are required to play.
 * Cards are the entities which are used to play the game.
 * They contain many fields which all come into play while playing the game.
 *
 * @ author (Ronnie van Meel)
 * @ version (1.0)
 */
class Card {
    private xPos: number;
    private yPos: number;

    private ownerColour: string;

    private symbol: string;
    private number: number;
    private id: string;

    private frontImage: string;
    private backImage: string;
    private backVariation : number;

    /**
     * Constructor for objects of class Card
     * Initializes all card information, both visual and back-end.
     */
    constructor(symbol: string, number: number, ownerColour: string) {
        this.xPos = 0;
        this.yPos = 0;

        this.ownerColour = ownerColour;

        this.symbol = symbol;
        this.number = number;
        this.id = symbol + number;

        this.frontImage = "./assets/images/cardfront/card" + symbol + number + ".png";
        this.backVariation = randomMinMax(1,4);
        this.backImage = "./assets/images/cardback/card" + ownerColour + this.backVariation + ".png";
    }

    /**
     * Updates the position of a card using x,y coordinates.
     * @ param xPos The new x position of a card.
     * @ param yPos The new y position of a card.
     */
    updatePosition = function(xPos: number, yPos: number) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    /**
     * Updates the visual card back of a card.
     * @ param newOwnerColour The new colour of the card back, depending on posessor of the card.
     */
    updateCardBack = function(newOwnerColour: string) {
        this.ownerColour = newOwnerColour;
        this.backImage = "./assets/images/cardback/card" + this.ownerColour + this.backVariation + ".png";
    }

    /**
     * Various functions, getters and setters all returning small pieces of information regarding the Card's look and number.
     */
    get posiX(): number {
        return this.xPos;
    }

    get posiY(): number {
        return this.yPos;
    }

    get cardFront(): string {
        return this.frontImage;
    }

    set cardFront(newUrl: string) {
        this.frontImage = newUrl;
    }

    get cardBack(): string {
        return this.backImage;
    }

    get cardSymbol(): string {
        return this.symbol;
    }

    get cardNumber(): number {
        return this.number;
    }

    get cardId(): string {
        return this.id;
    }
}