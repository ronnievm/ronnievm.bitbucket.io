/**
 * Player class which represents you (the player) in the game.
 * Contains many objects, arrays and other fields which are required to play.
 *
 * @ author (Ronnie van Meel)
 * @ version (1.0)
 */
class Player {
    private playerHand: Card[];
    private points: number;

    /**
     * Constructor for objects of class Player.
     * Initializes the Player's hand and score.
     */
    constructor() {
        this.playerHand = [];
        this.points = 0;
    }

    /**
     * Various functions, getters and setters all returning small pieces of information regarding the Player's hand and score.
     */
    addCard = function(drawnCard: Card) {
        this.playerHand.push(drawnCard);
    }

    addPoint = function() {
        ++this.points;
    }

    removeCard = function(id: string) {
        for(var i = 0; i < this.playerHand.length; ++i) {
            if(id == this.playerHand[i].cardId) {
                return this.playerHand.splice(i, 1);
            }
        }
    }

    returnToRemoveCard = function(id: string) {
        for (var i = 0; i < this.playerHand.length; ++i) {
            if(id == this.playerHand[i].cardId) {
                return this.playerHand[i];
            }
        }
    }

    returnCard = function(index: number) {
        return this.playerHand[index];
    }

    returnCards = function() {
        return this.playerHand;
    }

    get handSize(): number {
        return this.playerHand.length;
    }

    get playerPoints(): number {
        return this.points;
    }
}