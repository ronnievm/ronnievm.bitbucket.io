/**
 * AI class which represents the opponent in this game.
 * Contains many objects, arrays and other fields which are required to play.
 *
 * @ author (Ronnie van Meel)
 * @ version (1.0)
 */
class Ai {
    private aiHand: Card[];
    private points: number;

    /**
     * Constructor for objects of class Ai.
     * Initializes the AI's hand and score.
     */
    constructor() {
        this.aiHand = [];
        this.points = 0;
    }

    /**
     * Various functions, getters and setters all returning small pieces of information regarding the AI's hand and score.
     */
    addCard = function(drawnCard: Card) {
        this.aiHand.push(drawnCard);
    }

    addPoint = function() {
        ++this.points;
    }

    removeCard = function() {
        return this.aiHand.splice(0, 1);
    }

    returnCard = function(index: number) {
        return this.aiHand[index];
    }

    returnCards = function() {
        return this.aiHand;
    }

    get handSize(): number {
        return this.aiHand.length;
    }

    get playerPoints(): number {
        return this.points;
    }
}