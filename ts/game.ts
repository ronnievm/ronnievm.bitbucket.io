/// <reference path="random.ts" />
/// <reference path="card.ts" />
/// <reference path="player.ts" />
/// <reference path="ai.ts" />


/**
 * Game class which handles and runs the main game.
 * Contains many objects, arrays and other fields which are required to play.
 * Contains many functions vital to the game's playability.
 * 
 * @ author (Ronnie van Meel)
 * @ version (1.0)
 */
class Game {
    private cardSymbols: any[];
    private deck: Card[];
    private graveyard: Card[];

    private player: Player;
    private ai: Ai;

    private toRemove: string;

    private playerScore: number;
    private aiScore: number;
    private playerTurn: boolean;

    /**
     * Constructor for objects of class Game.
     * Initializes cards, the deck, players, scores etc.
     */
    constructor() {
        this.cardSymbols = [];
        this.deck = [];
        this.graveyard = [];

        this.player = new Player();
        this.ai = new Ai();

        this.toRemove = "";
        this.playerScore = 0;
        this.aiScore = 0;
        this.playerTurn = true;
    }

    /**
     * Starter function which initializes all important aspects of the game.
    */
    init = function() {
        this.populate();
        this.draw();
        this.update();
    }

    /**
     * Fills the deck array with all playable cards.
     * 12 cards per suit.
    */
    populate = function() {
        this.cardSymbols.push("Clubs", "Diamonds", "Hearts", "Spades");

        for(var x = 0; x < this.cardSymbols.length; ++x) {
            for(var y = 2; y < 15; ++y) {
                let nextCard = new Card(this.cardSymbols[x], y, "Grey");
                this.deck.push(nextCard);
            }
        }
    }   
    
    /**
     * Deals each player 4 cards respectively, or 2 if there's only 4 cards left near the end of the game.
     * Cards drawn are removed from the deck array using splice.
    */
    draw = function() {
        if(this.deck.length <= 0) {
            this.checkWinner();
        } else {
            if(this.deck.length >= 8) {
                for(var i = 0; i < 4; ++i) {
                    var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                    cardDrawn[0].updateCardBack("Blue");
                    this.player.addCard(cardDrawn[0]);
                }
        
                for(var i = 0; i < 4; ++i) {
                    var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                    cardDrawn[0].updateCardBack("Red");
                    this.ai.addCard(cardDrawn[0]);
                }
                this.createNotif("draw4");
            } else {
                for (var i = 0; i < 2; ++i) {
                    var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                    cardDrawn[0].updateCardBack("Blue");
                    this.player.addCard(cardDrawn[0]);
                }
    
                for(var i = 0; i < 2; ++i) {
                    var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                    cardDrawn[0].updateCardBack("Red");
                    this.ai.addCard(cardDrawn[0]);
                }
                this.createNotif("draw2");
                this.createNotif("deckout");
            }
        }      
    }

    /**
     * Drag & drop events allowing the player to drag the card they wish to play onto the field.
     * Once dropped, various elements appear or disappear, depending on drop location.
     * (Dis)appareance of these elements is to prevent the player messing up the game.
     * @ param event The drag & drop event data the player gives.
    */
    drag = function(event: any) {
        event.dataTransfer.setData("text", event.target.id);
    }

    allowDrop = function(event: any) {
        event.preventDefault();
    }

    drop = function(event: any) {
        if(this.playerTurn) {
            event.preventDefault();
            var data = event.dataTransfer.getData("text");
            event.currentTarget.appendChild(document.getElementById(data));

            if(event.currentTarget.id == "temp") {
                document.getElementById(data).removeAttribute("cardsmall");
                document.getElementById(data).setAttribute("class", "cardbig");
    
                document.getElementById("temp").removeAttribute("ondrop");
                document.getElementById("temp").removeAttribute("ondragover");
    
                document.getElementById("confirm").classList.remove("invisible");
                document.getElementById("choiceform").classList.remove("invisible");
    
                this.createNotif("drop");
                this.toRemove = (event.srcElement).children[0].id;
            } else {
                document.getElementById(data).removeAttribute("cardbig");
                document.getElementById(data).setAttribute("class", "cardsmall");
    
                document.getElementById("temp").setAttribute("ondrop", "game.drop(event)");
                document.getElementById("temp").setAttribute("ondragover", "game.allowDrop(event)");
    
                if(document.getElementById("temp").hasChildNodes() == false) {
                    document.getElementById("choiceform").classList.add("invisible");
                    document.getElementById("confirm").setAttribute("class", "invisible");
                }                   
            }
        }
    }

    /**
     * The discard function which moves played cards towards the graveyard.
     * Has two branches depending on whose turn it is.
     * The AI branch has minor choice RNG.
     * The AI branch also has code to change the names of picture cards in the logs, as they are numbered 11-13.
     * If both player's hands are empty, the draw() function is called to refill.
    */
    discard = function() {
        var truth = document.getElementById("choicetruth") as HTMLFormElement;
        var lie = document.getElementById("choicelie") as HTMLFormElement;

        if(this.playerTurn) {
            if(truth.checked || lie.checked) {
                this.aiGuess();
                var toDiscard = this.player.removeCard(this.toRemove);
                this.graveyard.push(toDiscard[0]);
                this.update();
        
                document.getElementById("temp").setAttribute("ondrop", "game.drop(event)");
                document.getElementById("temp").setAttribute("ondragover", "game.allowDrop(event)");

                var nextAiCard = this.ai.removeCard(); 
                this.graveyard.push(nextAiCard[0]);    

                document.getElementById("temp").appendChild(document.getElementById(nextAiCard[0].cardId));
                document.getElementById(nextAiCard[0].cardId).removeAttribute("cardsmall");
                document.getElementById(nextAiCard[0].cardId).setAttribute("class", "cardbig");

                if(document.getElementById("log").childElementCount >= 10) {
                    var list = document.getElementById("log");
                    list.removeChild(list.childNodes[0]);
                    list.removeChild(list.childNodes[0]);
                }

                this.createNotif("aidrop");
                var notification = document.createElement("div");
                notification.setAttribute("class", "notification");

                var choicePool = [];
                choicePool.push(...this.deck);
                choicePool.push(...this.graveyard);
                choicePool.push(...this.player.returnCards());

                if(randomMinMax(0, 1) == 0) {
                    var cardNumber = nextAiCard[0].cardNumber;
                    if(cardNumber > 10) {
                        switch (cardNumber) {
                            case 11:
                                cardNumber = "ace";
                                break;
                            case 12:
                                cardNumber = "jack";
                                break;
                            case 13:
                                cardNumber = "king";
                                break;
                            case 14:
                                cardNumber = "queen";
                            default:
                                break;
                        }
                    }
                    notification.textContent = "He claims it's the " + cardNumber + " of " + nextAiCard[0].cardSymbol + ".";
                    document.getElementById("log").appendChild(notification);
                } else {
                    var randomCard = choicePool[randomMinMax(0, choicePool.length -1)];
                    var randomCardNumber = randomCard.cardNumber;

                    if(randomCardNumber > 10) {
                        switch (randomCardNumber) {
                            case 11:
                                randomCardNumber = "ace";
                                break;
                            case 12:
                                randomCardNumber = "jack";
                                break;
                            case 13:
                                randomCardNumber = "king";
                                break;
                            case 14:
                                randomCardNumber = "queen";
                            default:
                                break;
                        }
                    }

                    notification.textContent = "He claims it's the " + randomCardNumber + " of " + randomCard.cardSymbol + ".";
                    document.getElementById("log").appendChild(notification);
                }

                this.playerTurn = false;
            }                 
        } else {
            if(truth.checked || lie.checked) {
                this.playerGuess();
                this.update();

                this.playerTurn = true;
            }
        }

        if(this.player.handSize <= 0 && this.ai.handSize <= 0) {
            this.draw();
        }
    }

    /**
     * Function determining the AI's guess and end result.
    */
    aiGuess = function() {
        var rng = randomMinMax(0,1);
        if(rng == 0) {
            this.playerScore++;
            this.createNotif("playerscore1");
            document.getElementById("playerscore").innerText = (this.playerScore).toString();
        } else {
            this.aiScore++;
            this.createNotif("aiscore1");
            document.getElementById("aiscore").innerText = (this.aiScore).toString();
        }
    }

    /**
     * Function determining the player's guess and end result.
    */
    playerGuess = function() {
        var choicePool = [];
        var test = this.player.returnCards();
        choicePool.push(...test);
        choicePool.push(...this.deck);

        var truth = document.getElementById("choicetruth") as HTMLFormElement;
        var lie = document.getElementById("choicelie") as HTMLFormElement;

        var aiChoiceBase;
        if(randomMinMax(0,1) == 0) {
            aiChoiceBase = true;
        } else {
            aiChoiceBase = false;
        }

        if(aiChoiceBase == true) {
            if(truth.checked) {
                this.playerScore++;
                this.createNotif("playerscore2");
            } else {
                this.aiScore++;
                this.createNotif("aiscore2");
            }
        } else {
            if(truth.checked) {
                this.aiScore++;
                this.createNotif("aiscore2");
            } else {
                this.playerScore++;
                this.createNotif("playerscore2");
            }
        }

        document.getElementById("playerscore").innerText = (this.playerScore).toString();
        document.getElementById("aiscore").innerText = (this.aiScore).toString();

        document.getElementById("choiceform").classList.add("invisible");
        document.getElementById("confirm").setAttribute("class", "invisible");
    }
    
    /**
     * Creates a text notification to let the player know what's happening in the game.
     * Notifications are pasted into the chat log in the upper right corner.
     * After the limit of 10, old notifications will be deleted and replaced for new ones.
    */
    createNotif(notifType: string) {
        if(document.getElementById("log").childElementCount >= 10) {
            var list = document.getElementById("log");
            list.removeChild(list.childNodes[0]);
        }

        var notification = document.createElement("div");
        notification.setAttribute("class", "notification");

        switch (notifType) {
            case "drop":
                notification.textContent = "You place a card face down.";
                break;
            case "aidrop":
                notification.textContent = "The AI places a card face down.";
                break;
            case "draw4":
                notification.textContent = "Both players draw four cards.";
                break;
            case "draw2":
                notification.textContent = "Both players draw two cards.";
                break;
            case "playerwin":
                notification.textContent = "Player won!";
                break;
            case "aiwin":
                notification.textContent = "Ai won!";
                break;
            case "tie":
                notification.textContent = "The match is a tie!";
                break;
            case "deckout":
                notification.textContent = "There are no more cards left in the deck.";
                break;
            case "playerscore1":
                notification.textContent = "The AI guesses wrong, you gain one point.";
                break;
            case "aiscore1":
                 notification.textContent = "The AI guesses right, he gains one point.";
                break;
            case "playerscore2":
                notification.textContent = "You guess right, you gain one point.";
                break;
            case "aiscore2":
                 notification.textContent = "You guess wrong, the AI gains one point.";
                break;
                       
            default:
                break;
        }

        document.getElementById("log").appendChild(notification);
    }

    /**
     * Function to determine which player is victorious.
     * The player with the highest score wins.
    */
    checkWinner() {
        if(this.playerScore > this.aiScore) {
            this.createNotif("playerwin");
        } else if(this.aiScore > this.playerScore) {
            this.createNotif("aiwin");
        } else {
            this.createNotif("tie");
        }
    }

    /**
     * Update function which redraws all visual elements in the game every time an action is made.
     * Cleans all child nodes of all elements and remakes elements based on contents of the new state of each array.
    */
    update = function() {
        var playerNodes = document.getElementById("playerhand");
        while(playerNodes.firstChild) {
            playerNodes.removeChild(playerNodes.firstChild);
        }

        var aiNodes = document.getElementById("aihand");
        while(aiNodes.firstChild) {
            aiNodes.removeChild(aiNodes.firstChild);
        }

        var deckNodes = document.getElementById("deck");
        while(deckNodes.firstChild) {
            deckNodes.removeChild(deckNodes.firstChild);
        }

        var tempNodes = document.getElementById("temp");
        while(tempNodes.firstChild) {
            tempNodes.removeChild(tempNodes.firstChild);
        }

        var graveNodes = document.getElementById("graveyard");
        while(graveNodes.firstChild) {
            graveNodes.removeChild(graveNodes.firstChild);
        }

        for(let i of this.player.returnCards()) {
            var card = document.createElement("img");
            card.setAttribute("class", "cardsmall");
            card.setAttribute("id", i.id);
            card.setAttribute("src", i.cardFront);
            card.addEventListener("dragstart", this.drag, false);
            card.style.left = i.posiX + "px";
            card.style.top = i.posY + "px";
            document.getElementById("playerhand").appendChild(card);
        }

        for(let i of this.ai.returnCards()) {
            var card = document.createElement("img");
            card.setAttribute("class", "cardsmall");
            card.setAttribute("id", i.id);
            card.setAttribute("src", i.cardBack);
            card.style.left = i.posiX + "px";
            card.style.top = i.posiY + "px";
            document.getElementById("aihand").appendChild(card);
        }

        if (this.deck.length > 0) {
            var cardDeck = document.createElement("img");
            cardDeck.setAttribute("class", "cardsmall");
            cardDeck.setAttribute("id", this.deck[this.deck.length - 1].id);
            cardDeck.setAttribute("src", this.deck[this.deck.length - 1].cardBack);
            cardDeck.style.left = this.deck[this.deck.length - 1].posiX + "px";
            cardDeck.style.top = this.deck[this.deck.length - 1].posiY + "px";
            document.getElementById("deck").appendChild(cardDeck);
        }

        if (this.graveyard.length > 0) {
            var cardGrave = document.createElement("img");
            cardGrave.setAttribute("class", "cardsmall");
            cardGrave.setAttribute("id", this.graveyard[this.graveyard.length - 1].id);
            cardGrave.setAttribute("src", this.graveyard[this.graveyard.length - 1].cardFront);
            cardGrave.style.left = this.graveyard[this.graveyard.length - 1].posiX + "px";
            cardGrave.style.top = this.graveyard[this.graveyard.length - 1].posiY + "px";
            document.getElementById("graveyard").appendChild(cardGrave);
        }
    }
}

/**
 * Tooltip function to display helpful messages when hovering the mouse over certain elements.
 * Did not get to implement this (yet).
*/
var tooltips = document.querySelectorAll('.tooltip span');
window.onmousemove = function (e) {
    var x = (e.clientX + 20) + 'px',
        y = (e.clientY + 20) + 'px';
    for (var i = 0; i < tooltips.length; i++) {
        (tooltips[i] as HTMLElement).style.top = y;
        (tooltips[i] as HTMLElement).style.left = x;
    }
};