var Ai = (function () {
    function Ai() {
        this.addCard = function (drawnCard) {
            this.aiHand.push(drawnCard);
        };
        this.addPoint = function () {
            ++this.points;
        };
        this.removeCard = function () {
            return this.aiHand.splice(0, 1);
        };
        this.returnCard = function (index) {
            return this.aiHand[index];
        };
        this.returnCards = function () {
            return this.aiHand;
        };
        this.aiHand = [];
        this.points = 0;
    }
    Object.defineProperty(Ai.prototype, "handSize", {
        get: function () {
            return this.aiHand.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ai.prototype, "playerPoints", {
        get: function () {
            return this.points;
        },
        enumerable: true,
        configurable: true
    });
    return Ai;
}());
var Card = (function () {
    function Card(symbol, number, ownerColour) {
        this.updatePosition = function (xPos, yPos) {
            this.xPos = xPos;
            this.yPos = yPos;
        };
        this.updateCardBack = function (newOwnerColour) {
            this.ownerColour = newOwnerColour;
            this.backImage = "./assets/images/cardback/card" + this.ownerColour + this.backVariation + ".png";
        };
        this.xPos = 0;
        this.yPos = 0;
        this.ownerColour = ownerColour;
        this.symbol = symbol;
        this.number = number;
        this.id = symbol + number;
        this.frontImage = "./assets/images/cardfront/card" + symbol + number + ".png";
        this.backVariation = randomMinMax(1, 4);
        this.backImage = "./assets/images/cardback/card" + ownerColour + this.backVariation + ".png";
    }
    Object.defineProperty(Card.prototype, "posiX", {
        get: function () {
            return this.xPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "posiY", {
        get: function () {
            return this.yPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "cardFront", {
        get: function () {
            return this.frontImage;
        },
        set: function (newUrl) {
            this.frontImage = newUrl;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "cardBack", {
        get: function () {
            return this.backImage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "cardSymbol", {
        get: function () {
            return this.symbol;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "cardNumber", {
        get: function () {
            return this.number;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Card.prototype, "cardId", {
        get: function () {
            return this.id;
        },
        enumerable: true,
        configurable: true
    });
    return Card;
}());
function randomMinMax(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
var Player = (function () {
    function Player() {
        this.addCard = function (drawnCard) {
            this.playerHand.push(drawnCard);
        };
        this.addPoint = function () {
            ++this.points;
        };
        this.removeCard = function (id) {
            for (var i = 0; i < this.playerHand.length; ++i) {
                if (id == this.playerHand[i].cardId) {
                    return this.playerHand.splice(i, 1);
                }
            }
        };
        this.returnToRemoveCard = function (id) {
            for (var i = 0; i < this.playerHand.length; ++i) {
                if (id == this.playerHand[i].cardId) {
                    return this.playerHand[i];
                }
            }
        };
        this.returnCard = function (index) {
            return this.playerHand[index];
        };
        this.returnCards = function () {
            return this.playerHand;
        };
        this.playerHand = [];
        this.points = 0;
    }
    Object.defineProperty(Player.prototype, "handSize", {
        get: function () {
            return this.playerHand.length;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "playerPoints", {
        get: function () {
            return this.points;
        },
        enumerable: true,
        configurable: true
    });
    return Player;
}());
var Game = (function () {
    function Game() {
        this.init = function () {
            this.populate();
            this.draw();
            this.update();
        };
        this.populate = function () {
            this.cardSymbols.push("Clubs", "Diamonds", "Hearts", "Spades");
            for (var x = 0; x < this.cardSymbols.length; ++x) {
                for (var y = 2; y < 15; ++y) {
                    var nextCard = new Card(this.cardSymbols[x], y, "Grey");
                    this.deck.push(nextCard);
                }
            }
        };
        this.draw = function () {
            if (this.deck.length <= 0) {
                this.checkWinner();
            }
            else {
                if (this.deck.length >= 8) {
                    for (var i = 0; i < 4; ++i) {
                        var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                        cardDrawn[0].updateCardBack("Blue");
                        this.player.addCard(cardDrawn[0]);
                    }
                    for (var i = 0; i < 4; ++i) {
                        var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                        cardDrawn[0].updateCardBack("Red");
                        this.ai.addCard(cardDrawn[0]);
                    }
                    this.createNotif("draw4");
                }
                else {
                    for (var i = 0; i < 2; ++i) {
                        var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                        cardDrawn[0].updateCardBack("Blue");
                        this.player.addCard(cardDrawn[0]);
                    }
                    for (var i = 0; i < 2; ++i) {
                        var cardDrawn = this.deck.splice(randomMinMax(0, this.deck.length - 1), 1);
                        cardDrawn[0].updateCardBack("Red");
                        this.ai.addCard(cardDrawn[0]);
                    }
                    this.createNotif("draw2");
                    this.createNotif("deckout");
                }
            }
        };
        this.drag = function (event) {
            event.dataTransfer.setData("text", event.target.id);
        };
        this.allowDrop = function (event) {
            event.preventDefault();
        };
        this.drop = function (event) {
            if (this.playerTurn) {
                event.preventDefault();
                var data = event.dataTransfer.getData("text");
                event.currentTarget.appendChild(document.getElementById(data));
                if (event.currentTarget.id == "temp") {
                    document.getElementById(data).removeAttribute("cardsmall");
                    document.getElementById(data).setAttribute("class", "cardbig");
                    document.getElementById("temp").removeAttribute("ondrop");
                    document.getElementById("temp").removeAttribute("ondragover");
                    document.getElementById("confirm").classList.remove("invisible");
                    document.getElementById("choiceform").classList.remove("invisible");
                    this.createNotif("drop");
                    this.toRemove = (event.srcElement).children[0].id;
                }
                else {
                    document.getElementById(data).removeAttribute("cardbig");
                    document.getElementById(data).setAttribute("class", "cardsmall");
                    document.getElementById("temp").setAttribute("ondrop", "game.drop(event)");
                    document.getElementById("temp").setAttribute("ondragover", "game.allowDrop(event)");
                    if (document.getElementById("temp").hasChildNodes() == false) {
                        document.getElementById("choiceform").classList.add("invisible");
                        document.getElementById("confirm").setAttribute("class", "invisible");
                    }
                }
            }
        };
        this.discard = function () {
            var truth = document.getElementById("choicetruth");
            var lie = document.getElementById("choicelie");
            if (this.playerTurn) {
                if (truth.checked || lie.checked) {
                    this.aiGuess();
                    var toDiscard = this.player.removeCard(this.toRemove);
                    this.graveyard.push(toDiscard[0]);
                    this.update();
                    document.getElementById("temp").setAttribute("ondrop", "game.drop(event)");
                    document.getElementById("temp").setAttribute("ondragover", "game.allowDrop(event)");
                    var nextAiCard = this.ai.removeCard();
                    this.graveyard.push(nextAiCard[0]);
                    document.getElementById("temp").appendChild(document.getElementById(nextAiCard[0].cardId));
                    document.getElementById(nextAiCard[0].cardId).removeAttribute("cardsmall");
                    document.getElementById(nextAiCard[0].cardId).setAttribute("class", "cardbig");
                    if (document.getElementById("log").childElementCount >= 10) {
                        var list = document.getElementById("log");
                        list.removeChild(list.childNodes[0]);
                        list.removeChild(list.childNodes[0]);
                    }
                    this.createNotif("aidrop");
                    var notification = document.createElement("div");
                    notification.setAttribute("class", "notification");
                    var choicePool = [];
                    choicePool.push.apply(choicePool, this.deck);
                    choicePool.push.apply(choicePool, this.graveyard);
                    choicePool.push.apply(choicePool, this.player.returnCards());
                    if (randomMinMax(0, 1) == 0) {
                        var cardNumber = nextAiCard[0].cardNumber;
                        if (cardNumber > 10) {
                            switch (cardNumber) {
                                case 11:
                                    cardNumber = "ace";
                                    break;
                                case 12:
                                    cardNumber = "jack";
                                    break;
                                case 13:
                                    cardNumber = "king";
                                    break;
                                case 14:
                                    cardNumber = "queen";
                                default:
                                    break;
                            }
                        }
                        notification.textContent = "He claims it's the " + cardNumber + " of " + nextAiCard[0].cardSymbol + ".";
                        document.getElementById("log").appendChild(notification);
                    }
                    else {
                        var randomCard = choicePool[randomMinMax(0, choicePool.length - 1)];
                        var randomCardNumber = randomCard.cardNumber;
                        if (randomCardNumber > 10) {
                            switch (randomCardNumber) {
                                case 11:
                                    randomCardNumber = "ace";
                                    break;
                                case 12:
                                    randomCardNumber = "jack";
                                    break;
                                case 13:
                                    randomCardNumber = "king";
                                    break;
                                case 14:
                                    randomCardNumber = "queen";
                                default:
                                    break;
                            }
                        }
                        notification.textContent = "He claims it's the " + randomCardNumber + " of " + randomCard.cardSymbol + ".";
                        document.getElementById("log").appendChild(notification);
                    }
                    this.playerTurn = false;
                }
            }
            else {
                if (truth.checked || lie.checked) {
                    this.playerGuess();
                    this.update();
                    this.playerTurn = true;
                }
            }
            if (this.player.handSize <= 0 && this.ai.handSize <= 0) {
                this.draw();
            }
        };
        this.aiGuess = function () {
            var rng = randomMinMax(0, 1);
            if (rng == 0) {
                this.playerScore++;
                this.createNotif("playerscore1");
                document.getElementById("playerscore").innerText = (this.playerScore).toString();
            }
            else {
                this.aiScore++;
                this.createNotif("aiscore1");
                document.getElementById("aiscore").innerText = (this.aiScore).toString();
            }
        };
        this.playerGuess = function () {
            var choicePool = [];
            var test = this.player.returnCards();
            choicePool.push.apply(choicePool, test);
            choicePool.push.apply(choicePool, this.deck);
            var truth = document.getElementById("choicetruth");
            var lie = document.getElementById("choicelie");
            var aiChoiceBase;
            if (randomMinMax(0, 1) == 0) {
                aiChoiceBase = true;
            }
            else {
                aiChoiceBase = false;
            }
            if (aiChoiceBase == true) {
                if (truth.checked) {
                    this.playerScore++;
                    this.createNotif("playerscore2");
                }
                else {
                    this.aiScore++;
                    this.createNotif("aiscore2");
                }
            }
            else {
                if (truth.checked) {
                    this.aiScore++;
                    this.createNotif("aiscore2");
                }
                else {
                    this.playerScore++;
                    this.createNotif("playerscore2");
                }
            }
            document.getElementById("playerscore").innerText = (this.playerScore).toString();
            document.getElementById("aiscore").innerText = (this.aiScore).toString();
            document.getElementById("choiceform").classList.add("invisible");
            document.getElementById("confirm").setAttribute("class", "invisible");
        };
        this.update = function () {
            var playerNodes = document.getElementById("playerhand");
            while (playerNodes.firstChild) {
                playerNodes.removeChild(playerNodes.firstChild);
            }
            var aiNodes = document.getElementById("aihand");
            while (aiNodes.firstChild) {
                aiNodes.removeChild(aiNodes.firstChild);
            }
            var deckNodes = document.getElementById("deck");
            while (deckNodes.firstChild) {
                deckNodes.removeChild(deckNodes.firstChild);
            }
            var tempNodes = document.getElementById("temp");
            while (tempNodes.firstChild) {
                tempNodes.removeChild(tempNodes.firstChild);
            }
            var graveNodes = document.getElementById("graveyard");
            while (graveNodes.firstChild) {
                graveNodes.removeChild(graveNodes.firstChild);
            }
            for (var _i = 0, _a = this.player.returnCards(); _i < _a.length; _i++) {
                var i = _a[_i];
                var card = document.createElement("img");
                card.setAttribute("class", "cardsmall");
                card.setAttribute("id", i.id);
                card.setAttribute("src", i.cardFront);
                card.addEventListener("dragstart", this.drag, false);
                card.style.left = i.posiX + "px";
                card.style.top = i.posY + "px";
                document.getElementById("playerhand").appendChild(card);
            }
            for (var _b = 0, _c = this.ai.returnCards(); _b < _c.length; _b++) {
                var i = _c[_b];
                var card = document.createElement("img");
                card.setAttribute("class", "cardsmall");
                card.setAttribute("id", i.id);
                card.setAttribute("src", i.cardBack);
                card.style.left = i.posiX + "px";
                card.style.top = i.posiY + "px";
                document.getElementById("aihand").appendChild(card);
            }
            if (this.deck.length > 0) {
                var cardDeck = document.createElement("img");
                cardDeck.setAttribute("class", "cardsmall");
                cardDeck.setAttribute("id", this.deck[this.deck.length - 1].id);
                cardDeck.setAttribute("src", this.deck[this.deck.length - 1].cardBack);
                cardDeck.style.left = this.deck[this.deck.length - 1].posiX + "px";
                cardDeck.style.top = this.deck[this.deck.length - 1].posiY + "px";
                document.getElementById("deck").appendChild(cardDeck);
            }
            if (this.graveyard.length > 0) {
                var cardGrave = document.createElement("img");
                cardGrave.setAttribute("class", "cardsmall");
                cardGrave.setAttribute("id", this.graveyard[this.graveyard.length - 1].id);
                cardGrave.setAttribute("src", this.graveyard[this.graveyard.length - 1].cardFront);
                cardGrave.style.left = this.graveyard[this.graveyard.length - 1].posiX + "px";
                cardGrave.style.top = this.graveyard[this.graveyard.length - 1].posiY + "px";
                document.getElementById("graveyard").appendChild(cardGrave);
            }
        };
        this.cardSymbols = [];
        this.deck = [];
        this.graveyard = [];
        this.player = new Player();
        this.ai = new Ai();
        this.toRemove = "";
        this.playerScore = 0;
        this.aiScore = 0;
        this.playerTurn = true;
    }
    Game.prototype.createNotif = function (notifType) {
        if (document.getElementById("log").childElementCount >= 10) {
            var list = document.getElementById("log");
            list.removeChild(list.childNodes[0]);
        }
        var notification = document.createElement("div");
        notification.setAttribute("class", "notification");
        switch (notifType) {
            case "drop":
                notification.textContent = "You place a card face down.";
                break;
            case "aidrop":
                notification.textContent = "The AI places a card face down.";
                break;
            case "draw4":
                notification.textContent = "Both players draw four cards.";
                break;
            case "draw2":
                notification.textContent = "Both players draw two cards.";
                break;
            case "playerwin":
                notification.textContent = "Player won!";
                break;
            case "aiwin":
                notification.textContent = "Ai won!";
                break;
            case "tie":
                notification.textContent = "The match is a tie!";
                break;
            case "deckout":
                notification.textContent = "There are no more cards left in the deck.";
                break;
            case "playerscore1":
                notification.textContent = "The AI guesses wrong, you gain one point.";
                break;
            case "aiscore1":
                notification.textContent = "The AI guesses right, he gains one point.";
                break;
            case "playerscore2":
                notification.textContent = "You guess right, you gain one point.";
                break;
            case "aiscore2":
                notification.textContent = "You guess wrong, the AI gains one point.";
                break;
            default:
                break;
        }
        document.getElementById("log").appendChild(notification);
    };
    Game.prototype.checkWinner = function () {
        if (this.playerScore > this.aiScore) {
            this.createNotif("playerwin");
        }
        else if (this.aiScore > this.playerScore) {
            this.createNotif("aiwin");
        }
        else {
            this.createNotif("tie");
        }
    };
    return Game;
}());
var tooltips = document.querySelectorAll('.tooltip span');
window.onmousemove = function (e) {
    var x = (e.clientX + 20) + 'px', y = (e.clientY + 20) + 'px';
    for (var i = 0; i < tooltips.length; i++) {
        tooltips[i].style.top = y;
        tooltips[i].style.left = x;
    }
};
var game = new Game();
game.init();
//# sourceMappingURL=main.js.map